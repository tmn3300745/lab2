import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from collections import namedtuple
import time
import os


# read data and split them to train and test parts
def prepare_data(path, test_size, random_state=322):
    data = pd.read_csv(path).astype("float32")
    
    x = data.drop(data.columns[-1],axis=1)
    y = data.iloc[:,-1]
    
    return train_test_split(x, y, test_size=test_size, random_state=random_state)


# Logistic loss
def logistic_loss(y_actual, y_predicted):
    return tf.math.log1p(tf.exp(-y_actual * y_predicted))


# Binary Crossentropy
def binary_crossentropy(y_actual, y_predicted):
    return -(y_actual * tf.math.log(y_predicted) + (1 - y_actual) * tf.math.log(1 - y_predicted))


# Adaboost loss
def adaboost_loss(y_actual, y_predicted):
    return tf.exp(-y_actual * y_predicted)


# create logistic regresseion model
def create_model(shape, loss_function, activation_function="sigmoid", optimizer="adam", metrics=["accuracy"], use_bias=True):
    model = tf.keras.Sequential([
        tf.keras.layers.Input(shape=(shape,)),
        tf.keras.layers.Dense(1, activation=activation_function, use_bias=use_bias)
    ])

    model.compile(optimizer=optimizer, loss=loss_function, metrics=metrics)

    return model


# train model and get data of losses
def train_model(model, x_train, y_train, x_test, y_test, epochs=100, batch_size=10):
    data_fit = model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, validation_data=(x_test, y_test))
    
    trained_model = Model(model, data_fit.history['loss'], data_fit.history['val_loss'])

    return trained_model

    
# build graphics of train and test losses
def build_graphic(models, dest):
    plt.figure(figsize=(12, 6))
    for name, model in models.items():
        plt.plot(model.loss_train, label=f"Train Loss ({name})")
        plt.plot(model.loss_test, label=f"Test Loss ({name})")

    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.title("Loss Curves")
    plt.legend()
    plt.grid(True)
    plt.savefig(f'{dest}/losses.png')


# measure accuracy of model
def measure_accuracy(model, x_test, y_test):
    y_prediction = model.predict(x_test)
    y_prediction = (y_prediction > 0.5).astype(int)
    
    return accuracy_score(y_test, y_prediction)


Model = namedtuple("Model", "model, loss_train, loss_test")

    
if __name__ == "__main__":
    x_train, x_test, y_train, y_test = prepare_data("./data/data_banknote_authentication.csv", 0.3)
    draft_models = {
        "Logistic_loss": create_model(x_train.shape[1], logistic_loss, activation_function='sigmoid'),
        "Binary_Crossentropy": create_model(x_train.shape[1], binary_crossentropy, activation_function='sigmoid'),
        "Adaboost_loss": create_model(x_train.shape[1], adaboost_loss, activation_function='sigmoid')
    }
    
    models = {}
    for name, model in draft_models.items():
        models[name] = train_model(model, x_train, y_train, x_test, y_test, batch_size=5)
        
    build_graphic(models, "./output")
    
    timestr = time.strftime("%Y%m%d-%H%M%S")
    with open(f'./output/{timestr}.txt', 'w') as f:
        for name, model in models.items():
            f.write(f"{name} -> Accuruacy = {measure_accuracy(model.model, x_test, y_test)}\n")
